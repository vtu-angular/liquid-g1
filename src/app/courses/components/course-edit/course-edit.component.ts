import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { take } from 'rxjs/operators';
import { Category } from '../../../categories/models/category.model';
import { CategoriesService } from '../../../categories/services/categories.service';
import { Course } from '../../models/course.model';
import { CoursesService } from '../../services/courses.service';

@Component({
  selector: 'lq-course-edit',
  templateUrl: './course-edit.component.html',
  styleUrls: ['./course-edit.component.scss']
})
export class CourseEditComponent implements OnInit {

  id: number;
  course: Course;

  categories: Category[];

  formGroup: FormGroup;

  constructor(private coursesService: CoursesService,
              private categoriesService: CategoriesService,
              private toastrService: ToastrService,
              private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder) {
    this.id = +this.route.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    if (this.id) {
      this.coursesService.getById$(this.id).pipe(
        take(1)
      ).subscribe((response) => {
        this.course = response;
        this.buildForm(response);
      });
    } else {
      this.buildForm();
    }

    this.categoriesService.getAll$().pipe(
      take(1)
    ).subscribe((response) => {
      this.categories = response;
    });
  }

  onSubmit(): void {
    if (this.formGroup.invalid) {
      this.formGroup.markAllAsTouched();

      return;
    }

    const body: Course = {
      ...this.course,
      ...this.formGroup.value
    };

    delete body.category;

    this.coursesService.save$(body).pipe(
      take(1)
    ).subscribe(() => {
      this.toastrService.success('Course was successfully saved.', 'Success');
      this.router.navigate(['courses']);
    });
  }

  private buildForm(course?: Course): void {
    if (!course) {
      course = new Course();
    }

    let publishAt;
    if (course.publishAt) {
      publishAt = new Date(course.publishAt);
    } else {
      publishAt = new Date();
    }

    this.formGroup = this.fb.group({
      title: [course.title, [Validators.required, Validators.minLength(3)]],
      subtitle: [course.subtitle, [Validators.required, Validators.minLength(3)]],
      description: [course.description, [Validators.minLength(3), Validators.maxLength(1000)]],
      categoryId: [course.categoryId, Validators.required],
      language: [course.language, Validators.required],
      price: [course.price, [Validators.required, Validators.min(0)]],
      discount: [course.discount, [Validators.min(0), Validators.max(100)]],
      posterImgUrl: course.posterImgUrl,
      isCertificateIncluded: course.isCertificateIncluded,
      publishAt: [publishAt, Validators.required]
    });
  }

}
