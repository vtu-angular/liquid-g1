import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { take } from 'rxjs/operators';
import { Course } from '../../models/course.model';
import { CoursesService } from '../../services/courses.service';

@Component({
  selector: 'lq-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.scss']
})
export class CourseDetailsComponent implements OnInit {

  id: number;
  course: Course;

  constructor(private coursesService: CoursesService,
              private toastrService: ToastrService,
              private router: Router,
              private route: ActivatedRoute) {
    this.id = +this.route.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.coursesService.getById$(this.id).pipe(
      take(1)
    ).subscribe((course) => {
      this.course = course;
    }, (response: HttpErrorResponse) => {
      this.toastrService.error(response.message, 'Error');
      this.router.navigate(['courses']);
    });
  }

  getCurrentPrice(): number {
    const regularPrice = this.course.price;
    const discount = this.course.discount; // percent

    return (regularPrice * (100 - discount)) / 100;
  }

}
