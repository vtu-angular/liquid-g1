import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { take } from 'rxjs/operators';
import { Course } from '../../models/course.model';
import { CoursesService } from '../../services/courses.service';

@Component({
  selector: 'lq-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.scss']
})
export class CoursesListComponent implements OnInit {

  courses: Course[];

  selectedCourse: Course;
  modalRef: BsModalRef;

  listType = 'table';

  constructor(private coursesService: CoursesService,
              private toastrService: ToastrService,
              private bsModalService: BsModalService) {
  }

  ngOnInit(): void {
    this.getAll();
  }

  openDeleteDialog(template: TemplateRef<any>, course: Course): void {
    this.selectedCourse = course;
    this.modalRef = this.bsModalService.show(template);
  }

  deleteCourse(): void {
    this.coursesService.delete$(this.selectedCourse.id).pipe(
      take(1)
    ).subscribe(() => {
      this.getAll();
      this.toastrService.success('Course was successfully deleted.', 'Success');
      this.modalRef.hide();
    }, (response: HttpErrorResponse) => {
      this.toastrService.error(response.message, 'Error', {
        disableTimeOut: true,
        closeButton: true
      });
    });
  }

  private getAll(): void {
    this.coursesService.getAll$().pipe(
      take(1)
    ).subscribe((response) => {
      this.courses = response;
    });
  }
}
