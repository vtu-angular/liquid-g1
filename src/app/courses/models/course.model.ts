import { Category } from '../../categories/models/category.model';

export class Course {
  id: number;
  created: Date;
  lastUpdated: Date;

  title: string;
  subtitle: string;
  description: string;
  categoryId: number;
  language: string;
  price: number;
  discount: number;
  isCertificateIncluded: boolean;
  posterImgUrl: string;
  publishAt: Date;

  category: Category;

  constructor() {
    this.language = 'English';
    this.price = 0;
    this.discount = 0;
    this.isCertificateIncluded = true;
  }
}
