import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SharedModule } from '../shared/shared.module';
import { CourseDetailsComponent } from './components/course-details/course-details.component';
import { CourseEditComponent } from './components/course-edit/course-edit.component';
import { CoursesListComponent } from './components/courses-list/courses-list.component';
import { CoursesRoutingModule } from './courses-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule,
    ButtonsModule.forRoot(),
    ModalModule.forChild(),
    SharedModule,
    CoursesRoutingModule
  ],
  declarations: [
    CoursesListComponent,
    CourseDetailsComponent,
    CourseEditComponent
  ],
  exports: [
    CoursesListComponent
  ]
})
export class CoursesModule {
}
