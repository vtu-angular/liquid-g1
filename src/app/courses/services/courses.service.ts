import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Course } from '../models/course.model';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(private httpClient: HttpClient) {
  }

  getAll$(): Observable<Course[]> {
    const url = environment.apiUrl + '/courses';

    const httpParams = new HttpParams({
      fromObject: {
        _expand: 'category'
      }
    });

    return this.httpClient.get<Course[]>(url, {
      params: httpParams
    });
  }

  getById$(id: number): Observable<Course> {
    const url = `${environment.apiUrl}/courses/${id}`;

    const httpParams = new HttpParams({
      fromObject: {
        _expand: 'category'
      }
    });

    return this.httpClient.get<Course>(url, {
      params: httpParams
    });
  }

  save$(course: Course): Observable<Course> {
    if (!course.id) {
      return this.create$(course);
    } else {
      return this.edit$(course);
    }
  }

  create$(course: Course): Observable<Course> {
    const url = environment.apiUrl + '/courses';

    course.created = new Date();
    course.lastUpdated = new Date();

    return this.httpClient.post<Course>(url, course);
  }

  edit$(course: Course): Observable<Course> {
    const url = `${environment.apiUrl}/courses/${course.id}`;

    course.lastUpdated = new Date();

    return this.httpClient.put<Course>(url, course);
  }

  delete$(id: number): Observable<void> {
    const url = `${environment.apiUrl}/courses/${id}`;

    return this.httpClient.delete<void>(url);
  }

}
